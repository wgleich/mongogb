# mongogb
Flask-MongoDB Guestbook Application all packaged up in docker.
Graphene, GraphQL dev playground

### Installing and Deploying
```
git clone https://github.com/willgleich/mongogb.git
cd mongogb
docker-compose up
```

### API
Interactive GraphIQL client is available at http://localhost/graphql<br/>
API endpoint (/graphql) is available and there are two current queries:
allPosts  
authorPosts (author: "name")

An example API query:
```
r = requests.post(
    'http://localhost/graphql',
    json={
        'query': '''
{allPosts
{
  post
  author
  time
}
}''',
    },
)

print(r.json()['data'])
```

Please note in the docker-compose.yml there are many development environment considerations, as this is a work in progress
