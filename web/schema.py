import graphene, os
import pymongo

# dbconfig
client = pymongo.MongoClient(
    os.environ['DB_PORT_27017_TCP_ADDR'],
    27017)
db = client.guestbook


class Post(graphene.ObjectType):
    _id = graphene.ID()
    post = graphene.String()
    author = graphene.String()
    time = graphene.types.datetime.DateTime()


class Query(graphene.ObjectType):
    hello = graphene.String(name=graphene.String(default_value="stranger"))
    all_posts = graphene.List(Post)
    author_posts = graphene.List(Post, author=graphene.String(default_value='wgleich'))

    def resolve_hello(self, info, name):
        return 'Hello ' + name

    def resolve_all_posts(self, info):
        return [Post(author=post['author'], post=post['post'], _id=post['_id'], time=post['time']) for post in
                db.posts.find()]

    def resolve_author_posts(self, info, author):
        return [Post(author=post['author'], post=post['post'], _id=post['_id'], time=post['time']) for post in
                db.posts.find({'author': author})]



schema = graphene.Schema(query=Query)
